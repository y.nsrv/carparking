package com.company.parking;

import java.util.Random;

public class Parking {

    private Random random = new Random();
    private int numberOfParkingSpaces;
    private Car[] parkingPlaces;
    private int minimalTimeToFreeParkingSpace = 0;
    private int howManyParkingSpacesLeft;

    public int getMinimalTimeToFreeParkingSpace() {
        return minimalTimeToFreeParkingSpace;
    }

    public int getHowManyParkingSpacesLeft() {
        return howManyParkingSpacesLeft;
    }

    public int getNumberOfParkingSpaces() {
        return numberOfParkingSpaces;
    }

    public Parking(int numberOfParkingSpaces) {
        this.numberOfParkingSpaces = numberOfParkingSpaces;
        this.howManyParkingSpacesLeft = numberOfParkingSpaces;
        this.parkingPlaces = new Car[numberOfParkingSpaces];
    }

    public boolean carDroveIn(Car car) {
        for (int i = 0; i < numberOfParkingSpaces; i++) {
            if (parkingPlaces[i] == null) {
                parkingPlaces[i] = car;
                this.howManyParkingSpacesLeft -= 1;
                car.howManyMovesLeft = random.nextInt(10) + 1;
                if ((minimalTimeToFreeParkingSpace != 0) && (car.howManyMovesLeft < minimalTimeToFreeParkingSpace)) {
                    minimalTimeToFreeParkingSpace = car.howManyMovesLeft;
                } else if (minimalTimeToFreeParkingSpace == 0) {
                    minimalTimeToFreeParkingSpace = car.howManyMovesLeft;
                }
                return true;
            }
        }
        return false;
    }

    public void clearParkingFromExpiredCars() {
        for (Car parkingPlace: parkingPlaces) {
            if (parkingPlace != null) {
                if (parkingPlace.howManyMovesLeft <= 0) {
                    parkingPlace = null;
                    this.howManyParkingSpacesLeft += 1;
                }
            }
        }
    }

    public void clearAllParkingSpaces() {
        for (int i = 0; i < numberOfParkingSpaces; i++) {
            parkingPlaces[i] = null;
        }
        howManyParkingSpacesLeft = numberOfParkingSpaces;
        minimalTimeToFreeParkingSpace = 0;
    }
}
