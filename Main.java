package com.company.parking;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Random random = new Random();
        PlateNumberGenerator plateNumberGenerator = new PlateNumberGenerator();

        int movesCounter = 0;
        boolean isEnd = false;
        Scanner scanner = new Scanner(System.in);

        boolean isEnteredNumberOk = false;
        int numberOfParkingSpaces = 0;
        while (!isEnteredNumberOk) {
            System.out.printf("Введите максимальное число мест на парковке: ");
            numberOfParkingSpaces = scanner.nextInt();

            if (numberOfParkingSpaces > 0) {
                isEnteredNumberOk = true;
            } else {
                System.out.println("Вы ввели неверное число, попробуйте снова");
            }
        }

        Parking parking = new Parking(numberOfParkingSpaces);
        System.out.println("Парковка начала свою работу");

        while (!isEnd) {
            System.out.println("Ход №" + ++movesCounter);
            int howManyCarsToGenerate = random.nextInt(numberOfParkingSpaces / 2) + 1;
            System.out.println("Хочет заехать " + howManyCarsToGenerate + " машин");
            int droveInCarsCounter = 0;

            boolean isParkingStillAcceptingCars = true;
            while ((isParkingStillAcceptingCars) && (droveInCarsCounter < howManyCarsToGenerate)) {
                Car car = new Car(plateNumberGenerator.getUniquePlateNumber());
                isParkingStillAcceptingCars = parking.carDroveIn(car);
                if (isParkingStillAcceptingCars) droveInCarsCounter++;
            }

            if (droveInCarsCounter != howManyCarsToGenerate) {
                System.out.println("На парковке не осталось мест, ближайшее место освободиться через " + parking.getMinimalTimeToFreeParkingSpace() + " ход(ов)");
            }

            boolean isContinue = false;
            while (!isContinue) {
                System.out.println("Введите номер следующего действия:");
                System.out.println("1. Завершить ход и перейти к следующему");
                System.out.println("2. Узнать сколько мест занято, сколько свободно, когда освободится ближайшее");
                System.out.println("3. Очистить парковку от всех машин");

                int actNumber = scanner.nextInt();

                switch (actNumber) {
                    case 1:
                        isContinue = true;
                        break;

                    case 2:
                        int howManySpacesBusy = parking.getNumberOfParkingSpaces() - parking.getHowManyParkingSpacesLeft();
                        System.out.println("Мест занято: " + howManySpacesBusy);
                        System.out.println("Мест свободно: " + parking.getHowManyParkingSpacesLeft());
                        System.out.println("Ходов до освобождения ближайшего места: " + parking.getMinimalTimeToFreeParkingSpace());
                        break;

                    case 3:
                        parking.clearAllParkingSpaces();
                        break;

                    default:
                        System.out.println("Неправильный номер комманды, введите номер еще раз!");
                }
            }

            parking.clearParkingFromExpiredCars();
        }
    }
}
