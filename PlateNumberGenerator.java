package com.company.parking;

public class PlateNumberGenerator {

    private int plateNumbersCounter = 0;

    public String getUniquePlateNumber() {
        int oldPlateNumberCounter = plateNumbersCounter;
        plateNumbersCounter++;
        if (oldPlateNumberCounter < 10) {
            return "000" + oldPlateNumberCounter;
        } else if (oldPlateNumberCounter < 100) {
            return "00" + oldPlateNumberCounter;
        } else if (oldPlateNumberCounter < 1000) {
            return "0" + oldPlateNumberCounter;
        } else {
            return "" + oldPlateNumberCounter;
        }
    }
}
